This bot helps us with managing our infrastructure and users here at ramnet.
There are a lot more features to come!

### How do I get set up? ###

* Install dependencies

_npm install_

* Create a config file

_cp config.js.sample config.js_

Edit with own values

* Run the server

_node server.js_