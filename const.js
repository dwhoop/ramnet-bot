// const.js
// ========
module.exports = {
    // About
    commit: 1,
    branch: "not defined",
    botName: "@ramnetBot",

    // Express
    localPort: 50443,

    // Misc
    vorstandGroup: "vorstand",
    adminGroup: "admin",
    fluradminGroup: "fluradmin",
    userGroup: "user",

    zabbixUrl: "https://monitor.ram.rwth-aachen.de/api_jsonrpc.php",
    zabbixApTemplateFile: "zabbix-ap-template.xml",
    unifiModelName: "UniFi UAP AC Pro (2. Gen)",
    zabbixDatabase: "RAM_monitor",
    allVlanedApIps: "all_vlaned_ap_ips",
    allNonVlanedUnifisIps: "all_non_vlaned_unifis_ips"
};
