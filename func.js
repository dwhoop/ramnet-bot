// func.js
// ========

var mysql = require('mysql'),
    config = require('./config'),
    bot = require('./bot'),
    git = require('git-rev'),
    constant = require('./const'),
    request = require('request');


var connection = mysql.createConnection({
    host: config.dbHost,
    user: config.dbUser,
    password: config.dbPassword,
    database: config.dbDefaultDB
});

git.short(function (str) {
    constant.commit = str;
})

git.branch(function (str) {
    constant.branch = str;
})

exports.zabbix = function (msg, call, params, callback) {

    // Consider reusing the session key
    // global.zabbixToken

    // Check if the api is there first
    request({
        headers: {
            'Content-Type': 'application/json'
        },
        uri: constant.zabbixUrl,
        body: JSON.stringify({
            "jsonrpc": "2.0",
            "method": "apiinfo.version",
            "id": 1,
            "auth": null,
            "params": {}
        }),
        method: 'POST'
    }, function (err, res, body) {
        if (!err && body != "") {
            parsed = JSON.parse(body);
            if (!(parsed || parsed.jsonrpc == "2.0")) {
                exports.reportError(31, msg, "Could not reach zabbix server at " + constant.zabbixUrl);
                return;
            }
        } else {
            exports.reportError(30, msg, "Internal error while contacting zabbix at " + constant.zabbixUrl, err);
            return;
        }
    });
    // Looks ok so far, zabbix api is there
    // Now we authenticate against the api

    request({
        headers: {
            'Content-Type': 'application/json'
        },
        uri: constant.zabbixUrl,
        body: JSON.stringify({
            "jsonrpc": "2.0",
            "method": "user.login",
            "params": {
                "user": config.zabbixUser,
                "password": config.zabbixPassword
            },
            "auth": null,
            "id": 2
        }),
        method: 'POST'
    }, function (err, res, body) {
        if (!err && body != "") {
            parsed = JSON.parse(body);
            if (parsed && parsed.error && parsed.error != "") {
                exports.reportError(32, msg, "Could not authenticate against zabbix api: " + parsed.error.message + " -> " + parsed.error.data);
                return;
            } else {
                // Login successfull
                console.log("Login to zabbix successfull");
                global.zabbixToken = parsed.result;

                // We have to do the subsequence calls here because after that function we get async calls
                // which then have no token in global.zabbixToken

                // Now lets do the calls
                request({
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    uri: constant.zabbixUrl,
                    body: JSON.stringify({
                        "jsonrpc": "2.0",
                        "method": call,
                        "params": params,
                        "auth": global.zabbixToken,
                        "id": 3
                    }),
                    method: 'POST'
                }, function (err, res, body) {
                    if (!err && body != "") {
                        parsed = JSON.parse(body);
                        if (parsed && parsed.result && parsed.result != "") {
                            callback(parsed.result);
                        } else if (parsed && parsed.error && parsed.error.message != "") {
                            exports.reportError(34, msg, "Running call <pre>" + call + "</pre> failed because: <pre>" + parsed.error.message + " " + parsed.error.data + "</pre>");
                        }
                    } else {
                        exports.reportError(33, msg, "Internal error while running the call <pre>" + call + "</pre> at <pre>" + constant.zabbixUrl + "</pre>", err);
                        return;
                    }
                });
            }
        } else {
            exports.reportError(33, msg, "Internal error while authenticating against api at " + constant.zabbixUrl, err);
            return;
        }
    });

};

exports.get = function (msg, group, query, callback) {
    //console.log(query);
    if (msg.from.username) {
        // User has username in telegram
        connection.query('SELECT `group` FROM user WHERE username_telegram="' + msg.from.username + '" LIMIT 1', function (err, rows) {
            if (!err) {
                // Query ok
                if (rows[0] && rows[0].group) {
                    // So sql did actually return anything
                    groupUserIsIn = rows[0].group;

                    // Autoformat screws this if up in format
                    if ((group == constant.userGroup && (groupUserIsIn == constant.userGroup || groupUserIsIn == constant.fluradminGroup || groupUserIsIn == constant.adminGroup || groupUserIsIn == constant.vorstandGroup)) || (group == constant.fluradminGroup && (groupUserIsIn == constant.fluradminGroup || groupUserIsIn == constant.adminGroup || groupUserIsIn == constant.vorstandGroup)) || (group == constant.adminGroup && (groupUserIsIn == constant.adminGroup || groupUserIsIn == constant.vorstandGroup)) || (group == constant.vorstandGroup && (groupUserIsIn == constant.vorstandGroup))) {
                        console.log(`Allowing access for ${msg.from.first_name} ${msg.from.last_name} (${msg.from.username}) with command "${msg.text}"`)
                        // All fine user has access
                        connection.query(query, callback);
                    } else {
                        // Not sufficient role rights
                        exports.reply(msg, "Sorry " + msg.from.first_name + " you only have " + rows[0].group + " access. This command can only be run by " + group);
                    }
                } else {
                    // sql did not find the user
                    exports.reportError(10, msg, "You have no username-telegram set in out database.\nSee /setTelegramUsername in /help", msg.from.username + " was not found in db");
                }
            } else {
                console.log("MySQL error: " + err);
            }
        })
    } else {
        // No username assigned in telegram
        exports.reportError(3, msg, "Looks like you have no username in telegram yet.\nGo \
            create one, then tell an admin to add it to our database\nSee \
            /setTelegramUsername in /help", "User has yet not username in telegram");
    }
};

exports.formatMacAddress = function (mac) {
    upperMac = mac.toUpperCase(); // convention in our database, makes aa -> AA
    var splitMac = []; //now we declare some vars which we need later, it works, don't judge
    var cleanMAC = "";

    // here we break up the MAC if it was devided by : - or . in our working var tempMAC
    if (upperMac.includes(":")) {
        splitMac = upperMac.split(":");
    } else if (upperMac.includes("-")) {
        splitMac = upperMac.split("-");
    } else if (upperMac.includes(".")) {
        splitMac = upperMac.split(".");
    } else if (!upperMac.includes(":") && !upperMac.includes("-") && !upperMac.includes(".") && upperMac.length == 12) {
        //if it wasn't devided and it was 12 chars (full mac without 0) than we split it manually
        for (i = 0; i < 6; i++) {
            splitMac[i] = upperMac.slice((i * 2), (i * 2 + 2)); //0,2  2,4 and so on
        }
    }
    //now we will fill up 0 if the packets (6*2) weren't full A B C D E F will be
    for (i = 0; i < splitMac.length; i++) {
        if (splitMac[i].length != 2) {
            splitMac[i] = 0 + splitMac[i];
        }
    } //  0A 0B 0C 0D 0E 0F

    //now we put everything back together in cleanMAC
    for (i = 0; i < splitMac.length; i++) {
        if (i != (splitMac.length - 1)) {
            cleanMAC += splitMac[i] + ":";
        } else {
            cleanMAC += splitMac[i];
        }
    }
    var regex = /^([0-9A-F]{2}[:]){5}([0-9A-F]{2})$/;
    //this is a regex for our DB specifically, CAPS and : as devider

    if (regex.test(cleanMAC) == false) {
        return null;
    } else { //now we have a full MAC which should be in line with our DB
        return cleanMAC;
    }
}

exports.chunk = function (str, n) {
    var ret = [];
    var i;
    var len;

    for (i = 0, len = str.length; i < len; i += n) {
        ret.push(str.substr(i, n))
    }

    return ret
};

exports.replyWithTimeout = function (msg, text, time) {
    setTimeout(function () {
        exports.reply(msg, text)
    }, time);
};

exports.reportErrorWithTimeout = function (errCode, msg, publicError, detail, time) {
    setTimeout(function () {
        exports.reportError(errCode, msg, publicError, detail)
    }, time);
};

exports.reportError = function (errCode, msg, publicError, detail) {
    if (!detail) detail = publicError;
    exports.reply(msg, "<code>Code: " + errCode + ", hint: </code>" + publicError);
    console.log("[Code:]" + errCode + " hint: " + detail);
};

exports.reply = function (toMessage, text, noMarkdown) {

    if (noMarkdown) {
        var promise = bot.sendMessage(toMessage.chat.id, text);
        promise.then(function () {
            // ok
            console.log("Send reply");
        }, function () {
            // failed
            exports.reportError(2, toMessage, "The reply message could not be send", "Error on sending: \n" + text);
        })
    } else {
        var promise = bot.sendMessage(toMessage.chat.id, text, {
            "parse_mode": 'HTML'
        });
        promise.then(function () {
            // Ok
            console.log("Send reply");
        }, function () {
            exports.reportError(1, toMessage, "The reply message is malformatted", "Malformatted: \n" + text);
        })

    }
};

exports.replyWithPhoto = function (toMessage, filepath, caption) {

    var promise = bot.sendPhoto(toMessage.chat.id, filepath, {
        "caption": caption
    });
    promise.then(function () {
        // Ok
        console.log("Send photo");
    }, function () {
        exports.reportError(1, toMessage, "The reply message (photo) is malformatted", "Malformatted: \n" + caption);
    })
};

exports.sendToTecGroup = function (text, noMarkdown) {
    var obj = {
        message_id: 0,
        from: {},
        chat: {
            id: config.tecGroupId
        },
        date: 0,
        text: '',
        entities: []
    };

    exports.reply(obj, text, noMarkdown)
};

exports.getDateTime = function () {
    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "." + month + "." + day + " " + hour + ":" + min + ":" + sec;
};

exports.currentDateTime = function () {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return day + "." + month + "." + year + " " + hour + ":" + min + ":" + sec;

}
