// backend.js
// ========

var express = require('express'),
    bodyParser = require('body-parser'),
    constant = require('./const'),
    config = require('./config'),
    bot = require('./bot');

var app = express();

///
/// All for the tcp backend endpoints
///

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}))

// parse application/json
app.use(bodyParser.json())

app.get('/', function(req, res) {
    res.send('Currently supported endpoints: <br> /tec  <br>POST: { "title": "SomeTitle", "text": "SomeLongerText" } <br> GET: ?title=Title&text=Text <br><br>');

});

app.post('/tec', function(req, res) {
    console.log("POST to /tec with " + JSON.stringify(req.body) + " from " + req.connection.remoteAddress);
    bot.sendMessage(config.tecGroupId, req.body.title + "\n" + req.body.text);
    res.send('{ "success": 1, "verb": POST }');
});

app.get('/tec', function(req, res) {
    console.log("GET to /tec with " + JSON.stringify(req.query) + " from " + req.connection.remoteAddress);
    bot.sendMessage(config.tecGroupId, req.query.title + "\n" + req.query.text);
    res.send('{ "success": 1, "verb": GET }');
});

app.listen(constant.localPort, function() {
    console.log('Bot endpoint listening on port ' + constant.localPort);
});


module.exports = app;
