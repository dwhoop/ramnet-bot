// commands.js
// ========

var func = require('./func'),
    constant = require('./const'),
    config = require('./config'),
    fs = require('fs');

var commands = [{
    name: "/getauth",
    description: "Return access rights",
    params: "",
    userLevel: constant.userGroup,
    func: function (msg, userLevel, params) {
        func.get(msg, userLevel, 'SELECT `group` FROM user WHERE username_telegram="' + msg.from.username + '"', function (err, rows) {
            if (!err) {
                console.log(rows);
                func.reply(msg, msg.from.first_name + ", you have <pre>" + rows[0].group + "</pre> rights")
            } else {
                func.reportError(6, msg, "MySQL", err);
            }
        });
    }
},

{ //properly commented
    name: "/adddeviceforuser", //name for function in chat
    description: "Add MAC for user.name with is_wifi flag",
    params: "mac(AA:BB:CC:DD:EE:FF) user.name ethernet(0)/wifi(1)/legacywifi(2)", //what the function excepts from you, every word one parameter 1. user.name 2.MAC
    userLevel: constant.fluradminGroup, //acces Level for this function
    func: function (msg, userLevel, params) {
        defQuery = 'SELECT COUNT(1) as "check" \
          FROM RAM.user \
          WHERE username = "' + params[1] + '"'; //returns 1 if user exists, 0 if they don't
        func.get(msg, userLevel, defQuery, function (err, rows) { //fetch reult from defQuery
            if (err) {
                func.reportError(14, msg, "There was a problem with the SQL query.", err); //standard Error, put this always in but change the number
                return;
            }
            if (rows[0].check == 0) { //see comment next du WHERE in defQuery
                func.reportError(15, msg, "Username doesn't exist, is not active or was written wrong.", err);
                return;
            } else { //okay, user exists, so we can move on
                var mac = params[0];
                if (mac.length > 17 || mac.length < 6) { //MAC shouldnt be longer than 17 characters 12:45:78:01:34:67 17 chars with :
                    func.reportError(16, msg, "MAC seems to be too long or too short.", err);
                    return;
                } else { //okay, MAC has about the right amount of characters, so we can move on
                    cleanMac = func.formatMacAddress(mac);
                    if (!cleanMac) {
                        func.reportError(112, msg, "The mac address could not be formatted. Stick to IEEE format.");
                    } else {

                        hostMac = cleanMac.replace(/:/g, '');
                        defQuery2 = 'SELECT COUNT(1) as "check", username   \
                                          FROM RAM_dhcp.devices \
                                          WHERE mac LIKE "' + cleanMac + '"';
                        func.get(msg, userLevel, defQuery2, function (err, rows) {
                            if (err) {
                                func.reportError(21, msg, "There was a problem with the SQL query.", err);
                                return;
                            }
                            if (rows[0] && rows[0].check == 1) {
                                func.reportError(22, msg, "Device already exists for user " + rows[0].username, err);
                                return;
                            } else { //it doesnt? okay, we can move
                                //preparing some vars for the INSERT query, important or some things will break here
                                var is_wifi = 0;
                                var is_legacy = 0;
                                if (params[2] == "1") {
                                    // wifi (802.11x) device
                                    is_wifi = 1;
                                } else if (params[2] == "2") {
                                    // legacy wifi device
                                    is_wifi = 1;
                                    is_legacy = 1;
                                } else {
                                    // ethernet client
                                    // variables preset above
                                }

                                defQuery1 = `
                                        INSERT INTO RAM_dhcp.devices (username, mac, is_wifi, is_legacy)
                                        VALUES ("${params[1]}", "${cleanMac}", ${is_wifi}, ${is_legacy})
                                    `;
                                func.get(msg, userLevel, defQuery1, function (err, rows) {
                                    if (err) {
                                        func.reportError(19, msg, "There was a problem with the SQL query.", err);
                                    } else {
                                        var wifi_message = "";
                                        if (is_wifi == 1) {
                                            if (is_legacy == 1) {
                                                wifi_message = " wifi (legacy)";
                                            } else {
                                                wifi_message = " wifi (802.11x)";
                                            }
                                        } else {
                                            wifi_message = " ethernet";
                                        }
                                        func.reply(msg, "✅ Added" + wifi_message + " device with MAC <pre>" + cleanMac + "</pre> for <pre>" + params[1] + "</pre>");
                                        func.sendToTecGroup("⚠️ " + msg.from.first_name + " (" + msg.from.username + ") added" + wifi_message + " device <pre>" + cleanMac + "</pre> to the account of <pre>" + params[1] + "</pre>");
                                    }

                                });


                            } // get2 function
                        }); // get

                    } //okay, MAC has about the right amount of characters, so we can move on


                }

            }
        });
    }
},

{
    name: "/settelegramusername",
    description: "Will set the username_telegram in database",
    params: "user.name username_telegram",
    userLevel: constant.adminGroup,
    func: function (msg, userLevel, params) {
        func.get(msg, userLevel, 'UPDATE user SET username_telegram="' + params[1] + '" WHERE username="' + params[0] + '";', function (err, rows) {
            if (!err) {
                if (rows && rows.affectedRows && rows.affectedRows == 1) {
                    func.reply(msg, "Set username_telegram to <pre>" + params[1] + "</pre> for <pre>" + params[0] + "</pre>");
                } else {
                    func.reportError(8, msg, "Username not in database");
                }
            } else {
                func.reportError(9, msg, "Something went wrong: " + err);
            }
        });
    }
},

{
    name: "/help",
    description: "Display this help page",
    params: "",
    userLevel: constant.userGroup,
    func: function (msg, userLevel, params) {
        compare = function (a, b) {
            if (a.name < b.name)
                return -1;
            if (a.name > b.name)
                return 1;
            return 0;
        }
        commands.sort(compare);

        var helpPage = "<b>ramnetBot\n   commit:" + constant.commit + "\n   branch:" + constant.branch + "</b>\n\n"
        helpPage += "Currently this version allows the following commands: \n"
        for (var i = 0; i < commands.length; i++) {
            if (!commands[i].hideInHelp) {
                helpPage += " -> " + commands[i].name + "\n";
                helpPage += "<pre>";
                helpPage += "  - Description:    " + commands[i].description + "\n";
                var params = (commands[i].params != null) ? commands[i].params : "";
                helpPage += "  - Usage:          " + commands[i].name + " " + params + "\n";
                helpPage += "  - Min user level: " + commands[i].userLevel + "\n";
                helpPage += "";
                helpPage += "</pre>";
            }
        }
        func.reply(msg, helpPage);
    }

},

{
    name: "/genbotcommandlist",
    description: "Generate command list to upgrade command list with BotFather",
    params: "",
    userLevel: constant.userGroup,
    func: function (msg, userLevel, params) {
        func.get(msg, userLevel, "", function (err, rows) {
            var helpPage = "Send this to BotFather after /setcommands \n\n"
            for (var i = 0; i < commands.length; i++) {
                if (!commands[i].hideInHelp) {
                    helpPage += commands[i].name.substring(1, commands[i].name.length) + " - ";
                    var params = (commands[i].params != null) ? commands[i].params : "";
                    helpPage += "(" + params + ") " + commands[i].description + "\n";
                }
            }
            func.reply(msg, helpPage, true);
        });
    }

},

{
    name: "/start",
    description: "Initial start, will only report /help",
    params: "",
    userLevel: constant.userGroup,
    hideInHelp: true,
    func: function (msg, userLevel, params) {
        func.get(msg, userLevel, "", function (err, rows) {
            func.reply(msg, "Welcome, please check out /help", true);
        });
    }

}

];

module.exports = commands;
