// bot.js
// ========

var config = require('./config'),
    TelegramBot = require('node-telegram-bot-api'),
    constant = require('./const'),
    commands = require('./commands'),
    func = require('./func'),
    http = require('http'),
    fs = require("fs");


var token = config.botToken;
var bot = new TelegramBot(token, {
    polling: true
});

global.state = 0;
global.waitingFor = "text";

Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};


bot.on('photo', function(msg) {
//    if (global.state == 1 && global.waitingFor == "image") {

//    } else {
        func.reportError(25, msg, "I do not know the context of the image. You need to call a command first.\nSee /help");
//    }
});

// Any kind of message
bot.on('text', function(msg) {
    //console.log(msg);
    if (msg.entities) {
        // bot command
        // We take length not length-1 to snap the blank
        var command = msg.text.substring(msg.entities[0].offset, msg.entities[0].length);
        var command = command.replace(constant.botName, "");

        // Exit if there are ongoing operations like images waiting
        if ((global.state != 0 || global.waitingFor != "text") && command != "/cancel")  {
            func.reportError(26, msg, "Looks like there are ongoing operations.\nWait or kill the current operation by sending /cancel\nThis might cause problems for other users using this bot at the moment.");
            return;
        }

        var params;
        var commandFound = false;
        for (var i = 0; i < commands.length; i++) {
            if (command == commands[i].name) {
                commandFound = true;
                var userLevel = commands[i].userLevel;
                var providedParams;
                if (commands[i].params.length != 0) {
                    // Command requires params
                    providedParams = msg.text.substring(msg.entities[0].offset + msg.entities[0].length + 1, msg.text.length);
                    providedParams = providedParams.split(" ");
                    providedParams = providedParams.clean("");
                    requiredParams = commands[i].params.split(" ");
                    if (providedParams.length != requiredParams.length) {
                        // So there are no params provided
                        func.reportError(5, msg, "You need to provide " + requiredParams.length + " arguments, not " + providedParams.length + "! \nSee /help");
                        return;
                    }
                }
                console.log("Running " + commands[i].name);
                global.params = providedParams;
                commands[i].func(msg, userLevel, providedParams);
            }
        }
        if (!commandFound) {
            func.reportError(4, msg, "Command not known. \nSee /help");
        }
    } else {
        // Normal text only
    }
});

module.exports = bot;
